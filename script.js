////////////// DECLARAÇÃO DE VARIÁVEIS ///////////////////////
const userPedra = document.getElementById('pedraBtn');
const userPapel = document.getElementById('papelBtn');
const userTesoura = document.getElementById('tesouraBtn');

const winnerCombinations = [
    [1,3], //pedra x tesoura
    [2,1], //papel x pedra
    [3,2]  //tesoura x papel
];

var userChoice = 0;
var choices = [];

var score = {
    user : 0,
    machine : 0,
    draws : 0
};



const userScore = document.getElementById('userScore');
const machineScore = document.getElementById('machineScore');
const drawsScore = document.getElementById('draws');
userScore.innerHTML = score.user;
machineScore.innerHTML = score.machine;
drawsScore.innerHTML = score.draws;

const userWin = document.getElementById('userWinner');
const machineWin = document.getElementById('machineWinner');



////////////// FUNÇÃO PRINCIPAL /////////////////////////////
function start(){
    let choicesCombination = getChoices();
    checkWinner(choicesCombination, winnerCombinations);
    reloadBoardscore (score);


}

////////////// RECEBE AS ESCOLHAS USER X MACHINE ///////////////
function getChoices(){
    if (userPedra.checked == false && userPapel.checked == false && userTesoura.checked == false){
        alert("[ERRO] Escolha uma opção!");
    } else {

        if (userPedra.checked){
            document.getElementById('userDisplay').src = "_images/pedra_esq.png";
            userChoice = 1;
        } else if(userPapel.checked) {
            document.getElementById('userDisplay').src = "_images/papel_esq.png";
            userChoice = 2;
        } else {
            document.getElementById('userDisplay').src = "_images/tesoura_esq.png";
            userChoice = 3;
        }


        choices = [];
        choices.push(userChoice);


        let machineChoice = getRandomInt(1,4);
        switch (machineChoice) {
            case 1: 
                document.getElementById('machineDisplay').src = "_images/pedra_dir.png";
                break;
            case 2: 
                document.getElementById('machineDisplay').src = "_images/papel_dir.png";
                break;
            case 3: 
                document.getElementById('machineDisplay').src = "_images/tesoura_dir.png";
                break;
        }
        choices.push(machineChoice);
        console.log(choices);
    }
    return choices;
}

////////////// GERA NÚMERO RANDOM ///////////////
// função que gera um número aleatório de [min a max[ --> getRandomInt(1, 3) -> 1,2
function getRandomInt(min, max) { 
    min = Math.ceil(min);                                 // A função Math.ceil(x) retorna o menor número inteiro maior ou igual a "x". Math.ceil(7.004); -> 8
    max = Math.floor(max);                                // Math.floor arredonda para inteiro. Arredonda sempre para baixo. Math.floor(4.9) -> 4
    return Math.floor(Math.random() * (max - min)) + min; // Math.random gera um número entre 0 - 1
}

////////////// VERIFICA VENCEDOR /////////////////////////////////////
function checkWinner(choicesCombination, winnerCombinations){
    if (choicesCombination[0] === choicesCombination[1]){
        console.log(choicesCombination[0], choicesCombination[1])
        score.draws++;
        machineWin.style.backgroundImage = "none";
        userWin.style.backgroundImage = "none";
    } else {
        for (let i = 0; i < winnerCombinations.length; i++){
            if (winnerCombinations[i][0] === choicesCombination[0] && winnerCombinations[i][1] === choicesCombination[1]){
                score.user++;
                // userWin.style.backgroundImage = "url(_images/coroa_soft.png)";
                // userWin.style.backgroundRepeat = "no-repeat";
                // userWin.style.backgroundPosition = "center";
                // machineWin.style.backgroundImage = "none";
                break;
            } else if (i === winnerCombinations.length -1){
                score.machine++;
                // userWin.style.backgroundImage = "none";
                // machineWin.style.backgroundImage = "url(_images/coroa_soft.png)";
                // machineWin.style.backgroundRepeat = "no-repeat";
                // machineWin.style.backgroundPosition = "center";
            }
        }
    }
    return score;
}


////////////// ATUALIZA O PLACAR /////////////////////////////////////
function reloadBoardscore (score){
    userScore.innerHTML = score.user;
    machineScore.innerHTML = score.machine;
    drawsScore.innerHTML = score.draws;
    if (score.user > score.machine){
        userWin.style.backgroundImage = "url(_images/crown.png)";
        userWin.style.backgroundRepeat = "no-repeat";
        userWin.style.backgroundPosition = "center";
        machineWin.style.backgroundImage = "none";
    } else if (score.user < score.machine){
        userWin.style.backgroundImage = "none";
        machineWin.style.backgroundImage = "url(_images/crown.png)";
        machineWin.style.backgroundRepeat = "no-repeat";
        machineWin.style.backgroundPosition = "center";
    } else if (score.user === score.machine){
        userWin.style.backgroundImage = "none";
        machineWin.style.backgroundImage = "none";
    }
}